# cahier_pdf.sh path/to/file.pdf '%s.cahier.pdf' 2
#				Fichier			 Format du nom	 Format de la brochure
# Le premier paramètre est le fichier à cahiétiser
# Le deuxième (optionnel) et le format à donner au nom de fichier fini
#	où %s représente le nom du fichier sans path ni extension
# Le troisième (optionnel) est le format de cahier à appliquer
#	2 - Cahier A5 imprimé sur A4 (4 pages par feuille A4 recto/verso)
#	3 - Dépliant A4 (6 pages par feuille A4 recto/verso)

function joinHalves {
	a=0

	while [ $a -lt $sides ]; do
		if [ $(($a % 2)) == 0 ]; then
			params="tmp_page$(($pages - $a)).pdf tmp_page$(($a + 1)).pdf"
		else
			params="tmp_page$(($a + 1)).pdf tmp_page$(($pages - $a)).pdf"
		fi
		pdfjam -q $params --nup $nup --landscape --outfile "tmp_new$a.pdf"
		rm $params
		let "a++"
	done
}

function joinThirds {
	a=0

	while [ $a -lt $sides ]; do
		params="tmp_page$(($pagesPerSide * $a + 1)).pdf tmp_page$(($pagesPerSide * $a + 2)).pdf tmp_page$(($pagesPerSide * $a + 3)).pdf"
		pdfjam -q $params --nup $nup --landscape -o "tmp_new$a.pdf"
		rm $params
		let "a++"
	done
}

function reUnite {
	a=1
	mv tmp_new0.pdf $newname
	while [ $a -lt $sides ]; do
		pdfjam -q "$newname" "tmp_new$a.pdf" --landscape -o "$newname"
		rm "tmp_new$a.pdf"
		let "a++"
	done
}


pages=$(pdfinfo "$1" | grep Pages | awk '{print $2}')

# on extrait le path et le nom du fichier (sans l'extension)
dir=$(dirname "$1")
filename=$(basename $1 ".pdf")
# on formatte avec le format passé en second argument
# ou %s.cahier.pdf s'il n'y a pas de deuxième argument
newformat="$dir/${2-"%s.cahier.pdf"}"
newname=$(printf $newformat $filename)
pagesPerSide=${3-2}
nup="${pagesPerSide}x1"
let "sides = pages / pagesPerSide"

# On sépare le PDF en plusieurs fichiers (un par page)
# Pour faciliter le traitement
pdfseparate "$1" 'tmp_page%d.pdf'

if [ $pagesPerSide == 2 ]; then
	if [ $(($pages % 2)) == 0 ]; then
		joinHalves
		reUnite
	else
		echo "ERREUR: Le nombre de pages n'est pas un multiple de 2. Impossible de faire un cahier avec ça."
	fi
elif [ $pagesPerSide == 3 ]; then
	if [ $(($pages % 3)) == 0 ]; then
		joinThirds
		reUnite
	else
		echo "ERREUR: Le nombre de pages n'est pas un multiple de 3. Impossible de faire un dépliant avec ça."
	fi
else
	echo "Format non supporté : $3"
fi

