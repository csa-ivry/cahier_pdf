# cahier_pdf.sh

## Un script minimal pour la mise-en-brochure !

Ce script permet de transformer un PDF page-par-page en une brochure mise en page pour impression recto-verso. Deux formats sont actuellement supportés :
* 2: Brochure en cahier A4 (4 pages A5 par feuille A4 recto/verso)
* 3: Brochure en dépliant A4 (6 pages par feuille A4 recto/verso)

NOTE: Ce script est profondément inutile si ton imprimante gère nativement le recto/verso et la création de cahier. Mais ce n'est pas le cas pour tout le monde !

## Syntaxe :

```
$ cahier_pdf.sh path/to/file.pdf '%s.cahier.pdf' 2
```

Le premier paramètre est le chemin d'accès au fichier à mettre en brochure.
Le second paramètre est le format pour le nom à donner au nouveau fichier, où %s est le nom du fichier sans extension et sans chemin d'accès.
Le troisième paramètre est le format de brochure à réaliser, où 2 est un cahier et 3 un dépliant.

## Licence

Script placé sous licence GNU GPL (voir le fichier LICENSE).

## Contribuer

Une idée ? Une suggestion ? N'hésite pas à ouvrir un ticket ou à envoyer directement un pull-request.
